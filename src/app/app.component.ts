import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Chat } from './chat.model';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  model!: Chat[];

  fg!: FormGroup;

  constructor(
    private chatService: ChatService, private fb: FormBuilder
    ) {}
  
  ngOnInit(): void {
    this.fg = this.fb.group({
      nom: [null, Validators.required],
      image: []
    });
    this.chatService.get().subscribe(data => this.model = data);
  }

  submit() {
    this.chatService.post(this.fg.value).subscribe(() => {
      this.chatService.get().subscribe(data => this.model = data);
    });
  }

  onChange($event: any) {
    let file = $event.target.files[0];
    console.log(file);

    let reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (e: any) => {
      this.fg.get('image')?.setValue(e.target.result);
    }
  }
}
