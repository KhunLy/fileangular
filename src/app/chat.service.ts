import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Chat } from './chat.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  

  constructor(
    private http: HttpClient
  ) { }

  get(): Observable<Chat[]>  {
    return this.http.get<Chat[]>(environment.api_url + '/chat');
  }

  post(value: Chat) {
    return this.http.post<any>(environment.api_url + '/chat', value);
  }
}
